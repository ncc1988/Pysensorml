#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    OWSCommon.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

from lxml import etree


class Section():
  def __init__(self):
    self.serviceIdentification = None
    self.serviceProvider = None
    self.operationMetadata = None
    self.contents = None
    self.all = None

class GetCapabilitiesRequest():
  def __init__(self):
    self.service = '' #service type identifier (mandatory)
    self.request = 'GetCapabilites' #has to be "GetCapabilites" (mandatory)
    self.acceptVersions = [] #array of string
    self.sections = [] #array of Section
    self.acceptFormats = [] #array of string
    self.updateSequence = '' #string
    self.acceptLanguages = [] #array of string
    
  def ParseXML(self, xmlstring):
    rootElement = etree.fromstring(xmlstring) #.getroot()
    if (rootElement.tag != '{http://www.opengis.net/ows/2.0}GetCapabilities'):
      #invalid request
      raise BaseException("Tag must be GetCapabilites, Tag is: "+rootElement.tag)
    self.request = 'GetCapabilites'
    self.service = rootElement.attrib.get('service')
    
    versions = rootElement.find('AcceptVersions')
    #print "versions:" + versions
    if(versions != None):
      for v in versions.findall('Version'):
        self.acceptVersions.append(v.text)
    
    sections = rootElement.find('Sections')
    if(sections != None):
      for s in sections.findall('Section'):
        self.sections.append(s.text)
    
    formats = rootElement.find('AcceptFormats')
    if(formats != None):
      for f in formats.findall('OutputFormat'):
        self.acceptFormats.append(f.text)
    
    languages = rootElement.find('AcceptLanguages')
    if(languages != None):
      for l in languages.findall('Language'):
        self.acceptLanguages.append(l.text)
    
    