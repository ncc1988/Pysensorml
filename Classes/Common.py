#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Common.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>#    
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

#This file contains common used constants and classes

NSMAP = {'sml': 'http://www.opengis.net/sensorML/1.0.1', 
         'gml': 'http://www.opengis.net/gml',
         'swe': 'http://www.opengis.net/swe/1.0',
         'xlink': 'http://www.w3.org/1999/xlink',
         'xsi':'http://www.w3.org/2001/XMLSchema-instance',
         'om':'http://www.opengis.net/om/2.0',
         'ows':'http://www.opengis.net/ows/2.0',

         }

URN = {
  "EPSG4329":"urn:ogc:crs:EPSG:4329",
  "Location":"urn:ogc:def:property:OGC:location",
  "Distance":"urn:ogc:dev:property:OGC:distance",
  "Length":"urn:ogc:def:property:OGC:length",
  
  "Interface":"urn:ogc:def:property:OGC:interface",
  
  "ElectricalCable":"urn:ogc:def:property:OGC:electricalCable",
  
  "WindSpeed":"urn:ogc:def:property:OGC:windSpeed",
  "WindDirection":"urn:ogc:def:property:OGC:windDirection",
  "Humidity":"urn:ogc:def:property:OGC:humidity",
  "Temperature":"urn:ogc:def:property:OGC:temperature",
  "Radiation":"urn:ogc:def:property:OGC:radiation",
  "Rainfall":"urn:ogc:def:property:OGC:rainfall",
  }