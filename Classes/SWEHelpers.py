#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    SWEHelpers.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de> 
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

from lxml import etree
from SensorML import *
from DataTypes import *
from Common import URN

#This file contains helper classes to create parts of a SensorML object hierarchy


class SWEPositionHelper:
  
  @classmethod
  def CreatePositionVectorLatLonAlt(self, gmlID, definition, lat, lon, alt):
    #lat and lon are latitude and longitude in degrees
    #alt is the altitude in Meter!
    return SWEVector(
      gmlID=gmlID,
      definition=definition,
      coordinates=[
        SWECoordinate(name="latitude",
          coordinate=SWEQuantity(
            definition="", uom="deg", value=lat)
          ),
        SWECoordinate(name="longitude",
          coordinate=SWEQuantity(
            definition="", uom="deg", value=lon)
          ),
        SWECoordinate(name="altitude",
          coordinate=SWEQuantity(
            definition="", uom="m", value=alt)
          )
        ]
      )
  
  @classmethod
  def CreateDistanceVectorXYZ(self, gmlID, definition, uom, x, y, z):
    return SWEVector(
      gmlID,
      definition,
      coordinates=[
        SWECoordinate(name="x",
          coordinate=SWEQuantity(definition="urn:ogc:dev:property:OGC:distance", uom=uom, value=x)
          ),
        SWECoordinate(name="y",
          coordinate=SWEQuantity(definition="urn:ogc:dev:property:OGC:distance", uom=uom, value=y)
          ),
        SWECoordinate(name="z",
          coordinate=SWEQuantity(definition="urn:ogc:dev:property:OGC:distance", uom=uom, value=z)
          )
        ]
      )
  
  @classmethod
  def CreatePosition_EPSG4329(self, localFrame, lat, lon, alt):
    vector = self.CreatePositionVectorLatLonAlt(
      str(localFrame)+"_POSITION",
      URN["Location"],
      lat, lon, alt)
    return SWEPosition(URN["EPSG4329"], "#"+str(localFrame), vector)
    
  @classmethod
  def CreateDistance_XYZ(self, referenceFrame, localFrame, uom, x, y, z):
    vector = self.CreateDistanceVectorXYZ(
      str(localFrame)+"_POSITION",
      URN["Distance"],
      uom,
      x, y, z)
    return SWEPosition("#"+str(referenceFrame), "#"+str(localFrame), vector)