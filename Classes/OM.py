
from lxml import etree
from Common import NSMAP
from DataTypes import DataType

class Observation():
  def __init__(self, identifier, name=None, description=None):
    self.identifier = identifier
    self.name = name
    self.description = description
    self.observationType = None
    self.phenomenomTime = None
    self.resultTime = None
    self.processLink = None #the (SensorML)-process from which the observation was made, as a link
    self.parameters = [] #parameters that are important for the observation (e.g. position)
    self.observedProperty = None #a link to the definition of the observed property
    self.featureOfInterest = None #a link to the feature of interest
    self.result = None #the result of the observation as SWE data type
  
  def SetResult(self, result):
    if (isinstance(result, DataType)):
      self.result = result
    else:
      raise Exception("SetResult: Result is not a data class!")
    
  def ToXML(self, parentNode=None):
    observationNode = None
    if(parentNode != None):
      observationNode = etree.SubElement(parentNode, "{http://www.opengis.net/om/2.0}OM_Observation", nsmap=NSMAP)
    else:
      observationNode = etree.Element("{http://www.opengis.net/om/2.0}OM_Observation", nsmap=NSMAP)
    observationNode.set("{http://www.opengis.net/gml}id", self.identifier)
    observationNode.set("{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", NSMAP['om']+' http://schemas.opengis.net/om/2.0/observation.xsd')
    
    if(self.name != None):
      nameNode = etree.SubElement(observationNode, "{http://www.opengis.net/gml}name", nsmap=NSMAP)
      nameNode.text = self.name
    if(self.description != None):
      descNode = etree.SubElement(observationNode, "{http://www.opengis.net/gml}description", nsmap=NSMAP)
      descNode.text = self.description
    if(self.observationType != None):
      typeNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}type", nsmap=NSMAP)
      typeNode.set("{http://www.w3.org/1999/xlink}href", self.observationType)
    if(self.phenomenomTime != None):
      phenNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}phenomenomTime", nsmap=NSMAP)
      self.phenomenomTime.ToXML(phenNode)
    if(self.resultTime != None):
      restNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}resultTime", nsmap=NSMAP)
      restNode.set("{http://www.w3.org/1999/xlink}href", self.resultTime)
    if(self.processLink != None):
      procNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}procedure", nsmap=NSMAP)
      procNode.set("{http://www.w3.org/1999/xlink}href", self.processLink)
    if(self.featureOfInterest != None):
      foiNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}featureOfInterest", nsmap=NSMAP)
      foiNode.set("{http://www.w3.org/1999/xlink}href", self.featureOfInterest.link)
      foiNode.set("{http://www.w3.org/1999/xlink}role", self.featureOfInterest.role)
    if(self.result != None):
      resultNode = etree.SubElement(observationNode, "{http://www.opengis.net/om/2.0}result", nsmap=NSMAP)
      self.result.ToXML(resultNode)
    return observationNode
  
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)