from lxml import etree

from Common import NSMAP, URN
from DataTypes import *
from Documents import SensorMLDocument


class SOSException(Exception):
  def __init__(self, exceptionCode, location):
    self.exceptionCode = exceptionCode
    self.location = location
    self.value = exceptionCode + "@"+self.location
  
  def __str__(self):
    return repr(self.value)

  def ToXML(self, parentNode=None):
    eNode = None
    if(parentNode != None):
      eNode = etree.SubElement(parentNode, "{http://www.opengis.net/ows/2.0}Exception", nsmap=NSMAP)
    else:
      eNode = etree.Element("{http://www.opengis.net/ows/2.0}Exception", nsmap=NSMAP)
    eNode.set("{http://www.opengis.net/ows/2.0}exceptionCode", self.exceptionCode)
    eNode.set("{http://www.opengis.net/ows/2.0}locator", self.location)
    return eNode
  
  def __str__(self):
    return self.ToXML()
    
    
class SOSExceptionReport():
  
  def __init__(self, exceptions=None):
    self.exceptions = exceptions
    
  
  def ToXML(self, parentNode=None):
    erNode = None
    if(parentNode != None):
      erNode = etree.SubElement(parentNode, "{http://www.opengis.net/ows/2.0}ExceptionReport", nsmap=NSMAP)
    else:
      erNode = etree.Element("{http://www.opengis.net/ows/2.0}ExceptionReport", nsmap=NSMAP)
    erNode.set("{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", "http://www.opengis.net/ows/2.0/ owsExceptionReport.xsd")
    #erNode.set("{xml}lang", "en") #TODO: find out how to set attributes of "XML namespace"
    erNode.set("{http://www.opengis.net/ows/2.0}version", "1.0.0")
    for e in self.exceptions:
      e.ToXML(erNode)
    return erNode
  
  def __str__(self):
    return self.ToXML()


class ServiceIdentification():
  def __init__(self, title, description, keywordList, serviceType, supportedServiceVersionList, serviceFees=None, accessConstrinats=None):
    self.title=title
    self.description = description
    self.keywordList = keywordList
    self.serviceType = serviceType
    self.supportedServiceVersionList = supportedServiceVersionList
    self.serviceFees = serviceFees
    self.accessConstrinats = accessConstrinats
  
  def ToXML(self, parentNode=None):
    

    
class SensorObservationService():
  
  def __init__(self, name, port, serviceIdentification, serviceProvider):
    self.name = name
    self.port = port
  
  
  
  def GetCapabilities(self):
    #GetCapabilities request handling method, returns XML code
    
    return ''
    
  def DescribeSensor(self, sensorId):
    #DescribeSensor request handling method
    pass
  
  def GetObservation(self, sensorId, observationParameters):
    #GetObservation request handling method
    pass
  