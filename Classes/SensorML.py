#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    SensorML.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>    
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

from lxml import etree
from DataTypes import *
from Common import NSMAP, URN
from SWEHelpers import SWEPositionHelper



class SensorMLException(Exception):
  def __init__(self, value):
    self.value = value
  
  def __str__(self):
    return repr(self.value)




class Input():
  def __init__(self, name, sweDataType=None):
    self.name = name
    if(isinstance(sweDataType, DataType)):
      self.sweDataType = sweDataType
    else:
      raise SensorMLException("Input constructor: Expected a DataType instance!")
    
  
  def ToXML(self, parentNode=None):
    inputNode = None
    if(parentNode != None):
      inputNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}input", nsmap=NSMAP)
    else:
      inputNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}input", nsmap=NSMAP)
    inputNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    
    if(self.sweDataType != None):
      self.sweDataType.ToXML(inputNode)
    
    return inputNode




class Output():
  def __init__(self, name, sweDataType=None):
    self.name = name
    self.sweDataType = sweDataType
  
  def ToXML(self, parentNode=None):
    outputNode = None
    if(parentNode != None):
      outputNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}output", nsmap=NSMAP)
    else:
      outputNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}output", nsmap=NSMAP)
    outputNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    
    if(self.sweDataType != None):
      self.sweDataType.ToXML(outputNode)
    return outputNode


class Parameter():
  def __init__(self, name, sweDataType=None):
    self.name = name
    if(isinstance(sweDataType, DataType)):
      self.sweDataType = sweDataType
    else:
      raise SensorMLException("Parameter constructor: Expected a DataType instance!")
    
  
  def ToXML(self, parentNode=None):
    inputNode = None
    if(parentNode != None):
      inputNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}parameter", nsmap=NSMAP)
    else:
      inputNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}parameter", nsmap=NSMAP)
    inputNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    
    if(self.sweDataType != None):
      self.sweDataType.ToXML(inputNode)
    
    return inputNode



class Interface():
  def __init__(self, name,
      mechanicalLayer=None,
      physicalLayer=None,
      datalinkLayer=None,
      networkLayer=None,
      transportLayer=None,
      sessionLayer=None,
      presentationLayer=None,
      applicationLayer=None,
      serviceLayer=None
      ):
    
    #TODO: check if *Layer objects are instances of SWEDataRecord or SWECategory!
    self.name = name
    self.mechLayer = mechanicalLayer
    self.physicalLayer = physicalLayer
    self.datalinkLayer = datalinkLayer
    self.networkLayer = networkLayer
    self.transportLayer = transportLayer
    self.sessionLayer = sessionLayer
    self.presentationLayer = presentationLayer
    self.appLayer = applicationLayer
    self.serviceLayer = serviceLayer
    
    
  
  def ToXML(self, parentNode=None):
    interfaceNode = None
    if(parentNode != None):
      interfaceNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}interface", nsmap=NSMAP)
    else:
      interfaceNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}interface", nsmap=NSMAP)
    interfaceNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    
    interfaceDefinitionNode = etree.SubElement(interfaceNode, "{http://www.opengis.net/sensorML/1.0.1}InterfaceDefinition")
    
    
    #TODO: Layer-Inhalte (mittels SWE-Klassen)
    applicationLayerNode = etree.SubElement(interfaceDefinitionNode, "{http://www.opengis.net/sensorML/1.0.1}applicationLayer")
    if(self.appLayer != None):
      self.appLayer.ToXML(applicationLayerNode)
    physicalLayerNode = etree.SubElement(interfaceDefinitionNode, "{http://www.opengis.net/sensorML/1.0.1}physicalLayer")
    if(self.physicalLayer != None):
      self.physicalLayer.ToXML(physicalLayerNode)
    mechanicalLayerNode = etree.SubElement(interfaceDefinitionNode, "{http://www.opengis.net/sensorML/1.0.1}mechanicalLayer")
    if(self.mechLayer != None):
      self.mechLayer.ToXML(mechanicalLayerNode)
    
    return interfaceNode




class Position():
  def __init__(self, name, positionDefinition=None):
    self.name = name
    #TODO: positionDefinition must be of Type SWEVector, SWEPosition or a SensorML Process
    if(isinstance(positionDefinition, SWEVector) or isinstance(positionDefinition, SWEPosition) or isinstance(positionDefinition, SensorML.Process)):
      self.positionDefinition = positionDefinition
    else:
      raise DataTypeException("Position initialisation: Position definition is not an instance of an accepted class!")
   
  
  def ToXML(self, parentNode=None):
    positionNode = None
    if(parentNode != None):
      positionNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}position", nsmap=NSMAP)
    else:
      positionNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}position", nsmap=NSMAP)
    positionNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    
    if(self.positionDefinition != None):
      self.positionDefinition.ToXML(positionNode)
    return positionNode




class Process():
  
  def __init__(self, name, gmlID=None, description=None):
    self.name = name
    if(gmlID == None):
      self.gmlID = name
    else:
      self.gmlID = gmlID
    self.description = description
    
    self.inputs = []
    self.outputs = []
    self.parameters = []
    
    self.interfaces = []
    self.connections = dict()
    self.processMethods = []
    self.components = []
    self.positions = []

    #for the following read the document:
    # http://www.sensorml.com/sensorML-2.0/examples/characteristics.html
    self.capabilities = [] #measurement ranges etc. goes in here
    self.characteristics = [] #battery life etc. goes in here 
    
  
  # The ToXML function has to be implemented by Component, System, ProcessChain and ProcessMethod!
  # It is "pure virtual" in the base class!
  def ToXML(self, parentNode=None):
    pass

  #overloaded in Component and ProcessModel to raise an exception:
  def AddProcessMethod(self, processMethod):
    #TODO: check if instance of ProcessMethod!
    self.processMethods.append(processMethod)
  
  #overloaded in Component and ProcessModel to raise an exception:
  def AddComponent(self, component):
    #TODO: check if instance of Component!
    self.components.append(component)
    
    #TODO: check if this is standard-compliant:
    #TODO: check for duplicates before adding an input/output!
    self.inputs += component.inputs
    self.outputs += component.outputs
    #self.connections.append(component.connections)
    
  def AddInput(self, smlInput):
    self.inputs.append(smlInput)
    #TODO: seek inputs to avoid double insertion of inputs.
  
  def AddOutput(self, smlOutput):
    self.outputs.append(smlOutput)
    #TODO: seek outputs to avoid double insertion of outputs.
  
  def AddParameter(self, smlParameter):
    self.parameters.append(smlParameter)
    #TODO: seek parameters to avoid double insertion of parameters.
  
  def CreateParameter(self, name, sweDataType):
    #creates a new parameter and appends it to the list of parameters
    self.parameters.append(Parameter(name, sweDataType))
  
  def AddCapability(self, capability):
    #TODO: check if SWE data class!
    self.capabilities.append(capability)
  
  def CreateCapability(self, name, capability):
    #creates an SWE field for that capability and appends it to the capabilities list
    self.capabilities.append(SWEField(name, capability))
  
  def AddCharacteristic(self, characteristic):
    #TODO: check if SWE data class!
    self.characteristics.append(characteristic)
  
  def CreateCharacteristic(self, name, characteristic):
    #creates an SWE field for that characteristic and appends it to the characteristics list
    self.characteristics.append(SWEField(name, characteristic))
    
  
  def AddInterface(self, smlInterface):
    self.interfaces.append(smlInterface)
    #TODO: seek interfaces to avoid double insertion of interfaces.
    
  
  def CreateConnection(self, name, smlInput, smlOutput):
    #to be verified:
    #seek inputs and outputs to avoid double insertion of inputs/outputs.
    #if an input and output is found it will be linked with the desired output.
    #if input or output or both aren't added yet they will be added to the input/output lists
    #and the connection will be stored
    if (smlInput not in self.inputs) and (smlOutput not in self.outputs):
      self.inputs.append(smlInput)
      self.outputs.append(smlOutput)
      self.connections[name] = [self.inputs.index(smlInput), self.outputs.index(smlOutput)]
    elif (smlInput in self.inputs) and (smlOutput not in self.outputs):
      self.outputs.append(smlOutput)
      self.connections[name] = [self.inputs.index(smlInput), self.outputs.index(smlOutput)]
    elif (smlInput not in self.inputs) and (smlOutput in self.outputs):
      self.inputs.append(smlInput)
      self.connections[name] = [self.inputs.index(smlInput), self.outputs.index(smlOutput)]
    else:
      #self.connections[self.inputs.index(smlInput)] = self.outputs.index(smlOutput)
      self.connections[name] = [self.inputs.index(smlInput), self.outputs.index(smlOutput)]
  
  
  def AddPosition(self, position):
    self.positions.append(position)
  
  
  def SetOwnPosition_EPSG4329(self, lat, lon, alt):
    self.AddPosition(
      SWEPositionHelper.CreatePosition_EPSG4329(str(self.name), lat, lon, alt)
      )
  
  def GetOwnPosition(self):
    pass
    
  def SetOwnDistance_XYZ(self, referenceProcess, uom, x, y, z):
    if(isinstance(referenceProcess, Process)):
      self.AddPosition(
        #TODO: use the already defined position of the reference process instead of referenceProcess.name!
        SWEPositionHelper.CreateDistance_XYZ(str(referenceProcess.name), str(self.name), uom, x, y, z)
        )
    else:
      raise SensorMLException("SetOwnDistance_XYZ: Reference process is not an instance of Process!")
  
  
  def ContentToXML(self, processNode):
    
    #gml-ID:
    processNode.set("{http://www.opengis.net/gml}id", self.gmlID)
    #name:
    #processNode.set("{http://www.opengis.net/sensorML/1.0.1}name", self.name)
    processName = etree.SubElement(processNode, "{http://www.opengis.net/gml}name")
    processName.text = self.name
    
    #optional metadata:
    if(self.description != None):
      processDescription = etree.SubElement(processNode, "{http://www.opengis.net/gml}description")
      processDescription.text = self.description
    
    #convert inputs:
    inputsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}inputs")
    inputListNode = etree.SubElement(inputsNode, "{http://www.opengis.net/sensorML/1.0.1}InputList")
    for i in self.inputs:
      i.ToXML(inputListNode)
    
    #convert outputs:
    outputsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}outputs")
    outputListNode = etree.SubElement(outputsNode, "{http://www.opengis.net/sensorML/1.0.1}OutputList")
    for o in self.outputs:
      o.ToXML(outputListNode)
    
    #convert parameters:
    parametersNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}parameters")
    parameterListNode = etree.SubElement(parametersNode, "{http://www.opengis.net/sensorML/1.0.1}ParameterList")
    for p in self.parameters:
      p.ToXML(parameterListNode)
    
    
    #INFO: capabilities and characteristics are described as one SWE DataRecord each
    
    #capabilities:
    capNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}capabilities")
    capNode.set("{http://www.opengis.net/sensorML/1.0.1}name", 'Process capabilities') #TODO: remove stub
    capListNode = etree.SubElement(capNode, "{http://www.opengis.net/swe/1.0}DataRecord", nsmap=NSMAP)
    for c in self.capabilities:
      c.ToXML(capListNode)
    
    #characteristics:
    charNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}characteristics")
    charNode.set("{http://www.opengis.net/sensorML/1.0.1}name", 'Process characteristics') #TODO: remove stub
    charListNode = etree.SubElement(charNode, "{http://www.opengis.net/swe/1.0}DataRecord", nsmap=NSMAP)
    for c in self.characteristics:
      c.ToXML(charListNode)
    
    
    #convert interfaces:
    interfacesNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}interfaces")
    interfaceListNode = etree.SubElement(interfacesNode, "{http://www.opengis.net/sensorML/1.0.1}InterfaceList")
    for i in self.interfaces:
      i.ToXML(interfaceListNode)
    
    #convert connections:
    connectionsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}connections")
    connectionListNode = etree.SubElement(connectionsNode, "{http://www.opengis.net/sensorML/1.0.1}ConnectionList")
    for c in self.connections.viewkeys():
      source=self.inputs[self.connections[c][0]].name
      destination=self.outputs[self.connections[c][1]].name
      
      connectionNode = etree.SubElement(connectionListNode, "{http://www.opengis.net/sensorML/1.0.1}connection")
      connectionNode.set("{http://www.opengis.net/sensorML/1.0.1}name", c)
      linkNode = etree.SubElement(connectionNode,"{http://www.opengis.net/sensorML/1.0.1}Link")
      sourceNode = etree.SubElement(connectionNode,"{http://www.opengis.net/sensorML/1.0.1}source")
      sourceNode.set("{http://www.opengis.net/sensorML/1.0.1}ref", "this/inputs/"+source)
      destinationNode = etree.SubElement(connectionNode,"{http://www.opengis.net/sensorML/1.0.1}destination")
      destinationNode.set("{http://www.opengis.net/sensorML/1.0.1}ref", "this/outputs/"+destination)
    
    """
    #convert components (link only!):
    componentsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}components")
    componentListNode = etree.SubElement(componentsNode, "{http://www.opengis.net/sensorML/1.0.1}ComponentList")
    
    for c in self.components:
      componentNode = etree.SubElement(componentListNode, "{http://www.opengis.net/sensorML/1.0.1}component")
      componentNode.set("{http://www.opengis.net/sensorML/1.0.1}name", c.name)
      componentNode.set("{http://www.w3.org/1999/xlink}role","urn:ogc:def:role:OGC:detector")
      componentNode.set("{http://www.w3.org/1999/xlink}href","this/"+c.gmlID)
    """
    
    #convert components (full output):
    componentsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}components")
    componentListNode = etree.SubElement(componentsNode, "{http://www.opengis.net/sensorML/1.0.1}ComponentList")
    
    for c in self.components:
      wrapperNode = etree.SubElement(componentListNode, "{http://www.opengis.net/sensorML/1.0.1}component", nsmap=NSMAP)
      wrapperNode.set("{http://www.opengis.net/sensorML/1.0.1}name", c.name)
      c.ToXML(wrapperNode)
    
    #positions:
    positionsNode = etree.SubElement(processNode, "{http://www.opengis.net/sensorML/1.0.1}positions")
    positionListNode = etree.SubElement(positionsNode, "{http://www.opengis.net/sensorML/1.0.1}PositionList")
    
    for p in self.positions:
      p.ToXML(positionListNode)




class Component(Process):
    
  def ToXML(self, parentNode=None):
    processNode = None
    if(parentNode != None):
      processNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}Component", nsmap=NSMAP)
    else:
      processNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}Component", nsmap=NSMAP)
    
    self.ContentToXML(processNode)
    return processNode
  
  def AddProcessMethod(self, processMethod):
    raise SensorMLException("Components must not have process methods!") #TODO: check with standard specification
  
  def AddComponent(sself, component):
    raise SensorMLException("Components must not have components!") #TODO: check with standard specification




class System(Process):
  
  def ToXML(self, parentNode=None):
    processNode = None
    if(parentNode != None):
      processNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}System", nsmap=NSMAP)
    else:
      processNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}System", nsmap=NSMAP)
    
    self.ContentToXML(processNode)
    
    return processNode
  
  



class ProcessMethod(Process):
    
  def ToXML(self, parentNode=None):
    processNode = None
    if(parentNode != None):
      processNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}ProcessMethod", nsmap=NSMAP)
    else:
      processNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}ProcessMethod", nsmap=NSMAP)
    
    self.ContentToXML(processNode)
    return processNode
  def AddProcessMethod(self, processMethod):
    raise SensorMLException("ProcessMethods must not have process methods as child processes!") #TODO: check with standard specification
  
  def AddComponent(sself, component):
    raise SensorMLException("ProcessMethods must not have components as child processes!") #TODO: check with standard specification




class ProcessChain(Process):
  
  def ToXML(self, parentNode=None):
    processNode = None
    if(parentNode != None):
      processNode = etree.SubElement(parentNode, "{http://www.opengis.net/sensorML/1.0.1}ProcessChain", nsmap=NSMAP)
    else:
      processNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}ProcessChain", nsmap=NSMAP)
    
    self.ContentToXML(processNode)
    
    return processNode
  



#class ProcessDocument renamed to SensorMLDocument and moved to Documents.py!




