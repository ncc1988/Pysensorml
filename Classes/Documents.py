#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Documents.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>#    
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

from lxml import etree
from SensorML import System, Component, ProcessChain, ProcessMethod
from Common import NSMAP

class SensorMLDocument():
  def __init__(self):
    self.members = []
  
  #TODO: check if instance of System, Component, ProcessChain or ProcessMethod!
  def AddProcess(self, process):
    self.members.append(process)
    
  def ToXML(self):
    rootNode = etree.Element("{http://www.opengis.net/sensorML/1.0.1}SensorML", nsmap=NSMAP)
    rootNode.set("{http://www.opengis.net/sensorML/1.0.1}version", "1.0.1")
    rootNode.set("{http://www.w3.org/2001/XMLSchema-instance}schemaLocation", "http://www.opengis.net/sensorML/1.0.1 http://schemas.opengis.net/sensorML/1.0.1/sensorML.xsd")
    
    for m in self.members:
      
      """
      for c in m.components:
        memberNode = etree.SubElement(rootNode,"{http://www.opengis.net/sensorML/1.0.1}member")
        #TODO: check if components are alyways detectors:
        memberNode.set("{http://www.w3.org/1999/xlink}role","urn:ogc:def:role:OGC:detector")
        c.ToSensorML(memberNode)
      """
      memberNode = etree.SubElement(rootNode,"{http://www.opengis.net/sensorML/1.0.1}member")
      #TODO: check for class instance of System and ProcessChain and set the xlink:role differently!
      memberNode.set("{http://www.w3.org/1999/xlink}role","urn:ogc:def:role:OGC:sensorSystem")
      m.ToXML(memberNode)
    
    return rootNode
    
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)