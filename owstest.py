from Classes.OWSCommon import *

g = GetCapabilitiesRequest()
#modified example from OGC Web Service Common Specification, Section 7.2.4:
g.ParseXML('<GetCapabilities xmlns="http://www.opengis.net/ows/2.0" xmlns:ows="http://www.opengis.net/ows/2.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/ows/2.0 fragmentGetCapabilitiesRequest.xsd" service="SOS" updateSequence="XYZ123" acceptLanguages="de-DE"><AcceptVersions><Version>1.0.0</Version></AcceptVersions><Sections><Section>Contents</Section></Sections><AcceptFormats><OutputFormat>text/xml</OutputFormat></AcceptFormats><AcceptLanguages><Language>en-CA</Language><Language>fr-CA</Language></AcceptLanguages></GetCapabilities>')

print vars(g)
