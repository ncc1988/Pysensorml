#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    DataTypes.py
#    This file is part of pySensorML
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#
#    pySensorML is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    pySensorML is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with pySensorML.  If not, see <http://www.gnu.org/licenses/>.
#

#This file contains data types defined in "SWE Common"

from lxml import etree
from Common import NSMAP


class DataTypeException(Exception):
  def __init__(self, value):
    self.value = value
  
  def __str__(self):
    return repr(self.value)



class DataType():
  def __init__(self, definition=None):
    self.definition = definition

  def ToXML(self):
    #All derived classes MUST implement this method! ... or else:
    raise DataTypeException('DataType: Function ToXML not implemented in derived class!')


class FeatureOfInterest():
  def __init__(self, link, role):
    self.link = link
    self.role = role
  
  #ToXML skipped here, no XML representation found until now
    
    

class SWEField(DataType):
  def __init__(self, name, content):
    self.name = name
    self.content = content
    #TODO: check if content is a DataType instance!
  
  def ToXML(self, parentNode=None):
    sweQuantityNode = None
    if(parentNode != None):
      sweQuantityNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}field", nsmap=NSMAP)
    else:
      sweQuantityNode = etree.Element("{http://www.opengis.net/swe/1.0}field", nsmap=NSMAP)
    sweQuantityNode.set("{http://www.opengis.net/swe/1.0}name", self.name)
    
    if(self.content != None):
      self.content.ToXML(sweQuantityNode)
    return sweQuantityNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)



class SWEDataRecord(DataType):
  def __init__(self, definition=None):
    self.definition = definition
    self.fields = dict()
  
  def AddField(self, fieldName, fieldContent):
    if(isinstance(fieldContent, DataType)):
      self.fields.update({fieldName:fieldContent})
    else:
      raise DataTypeException("DataRecord: Content is not a DataType class (or inherited class of DataType)!")
  
  def ToXML(self,parentNode=None):
    sweDataRecordNode = None
    if(parentNode != None):
      sweDataRecordNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}DataRecord", nsmap=NSMAP)
    else:
      sweDataRecordNode = etree.Element("{http://www.opengis.net/swe/1.0}DataRecord", nsmap=NSMAP)
    sweDataRecordNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition)
    
    for f in self.fields.viewkeys():
      sweDataRecordField = etree.SubElement(sweDataRecordNode, "{http://www.opengis.net/swe/1.0}field")
      sweDataRecordField.set("{http://www.opengis.net/swe/1.0}name", f)
      self.fields[f].ToXML(sweDataRecordField)
    
    return sweDataRecordNode

  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)
  

class SWEQuantity(DataType):
  def __init__(self, definition, uom=None, value=None):
    self.definition = definition
    self.uom = uom
    self.value = value
  
  def ToXML(self, parentNode=None):
    sweQuantityNode = None
    if(parentNode != None):
      sweQuantityNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}Quantity", nsmap=NSMAP)
    else:
      sweQuantityNode = etree.Element("{http://www.opengis.net/swe/1.0}Quantity", nsmap=NSMAP)
    sweQuantityNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition) #TODO: use real value instead of "null"
    
    if(self.uom != None):
      uomNode = etree.SubElement(sweQuantityNode, "{http://www.opengis.net/swe/1.0}uom")
      uomNode.set("{http://www.opengis.net/swe/1.0}code", self.uom)
    
    if(self.value != None):
      valueNode = etree.SubElement(sweQuantityNode, "{http://www.opengis.net/swe/1.0}value")
      valueNode.text = str(self.value)
    
    return sweQuantityNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)


class SWEQuantityRange(DataType):
  def __init__(self, definition, uom=None, minimum=None, maximum=None):
    self.definition = definition
    self.uom = uom
    self.minimum = minimum
    self.maximum = maximum
  
  def ToXML(self, parentNode=None):
    sweQuantityNode = None
    if(parentNode != None):
      sweQuantityNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}QuantityRange", nsmap=NSMAP)
    else:
      sweQuantityNode = etree.Element("{http://www.opengis.net/swe/1.0}QuantityRange", nsmap=NSMAP)
    sweQuantityNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition) #TODO: use real value instead of "null"
    
    if(self.uom != None):
      uomNode = etree.SubElement(sweQuantityNode, "{http://www.opengis.net/swe/1.0}uom")
      uomNode.set("{http://www.opengis.net/swe/1.0}code", self.uom)
    
    if((self.minimum != None) and (self.maximum != None)):
      valueNode = etree.SubElement(sweQuantityNode, "{http://www.opengis.net/swe/1.0}value")
      valueNode.text = str(self.minimum)+' '+str(self.maximum)
    
    return sweQuantityNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)


class SWECategory(DataType):
  def __init__(self, definition, value=None):
    self.definition = definition
    self.value = value
  
  def ToXML(self, parentNode=None):
    sweCategoryNode = None
    if(parentNode != None):
      sweCategoryNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}Category", nsmap=NSMAP)
    else:
      sweCategoryNode = etree.Element("{http://www.opengis.net/swe/1.0}Category", nsmap=NSMAP)
    sweCategoryNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition) #TODO: use real value instead of "null"
    
    if(self.value != None):
      valueNode = etree.SubElement(sweCategoryNode, "{http://www.opengis.net/swe/1.0}value")
      try:
        valueNode.text = str(self.value)
      except UnicodeEncodeError:
        print("Unicode decode error in string ["+self.value+"]")
        raise Exception("Unicode decode error in string ["+self.value+"]")
    return sweCategoryNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)


class SWEPosition(DataType):
  def __init__(self, referenceFrame, localFrame=None, location=None, orientation=None):
    self.referenceFrame = referenceFrame
    self.localFrame = localFrame
    self.location = location
    self.orientation = orientation
  
  def ToXML(self, parentNode=None):
    positionNode = None
    if(parentNode != None):
      positionNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}Position", nsmap=NSMAP)
    else:
      positionNode = etree.Element("{http://www.opengis.net/swe/1.0}Position", nsmap=NSMAP)
    positionNode.set("{http://www.opengis.net/swe/1.0}referenceFrame", self.referenceFrame)
    if(self.localFrame != None):
      positionNode.set("{http://www.opengis.net/swe/1.0}localFrame", self.localFrame)
    
    if(self.location != None):
      locationNode = etree.SubElement(positionNode, "{http://www.opengis.net/swe/1.0}location", nsmap=NSMAP)
      self.location.ToXML(locationNode)
    
    if(self.orientation != None):
      orientationNode = etree.SubElement(positionNode, "{http://www.opengis.net/swe/1.0}orientation", nsmap=NSMAP)
      self.orientation.ToXML(orientationNode)
    
    return positionNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)
  

class SWECoordinate(DataType):
  def __init__(self, name, coordinate):
    self.name = name
    if(isinstance(coordinate, SWEQuantity)):
      self.coordinate = coordinate
    else:
      raise DataTypeException("Coordinate initialisation: Coordinate not an instance of Quantity class!")
  
  def ToXML(self, parentNode=None):
    coordinateNode = None
    if(parentNode != None):
      coordinateNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}coordinate", nsmap=NSMAP)
    else:
      coordinateNode = etree.Element("{http://www.opengis.net/swe/1.0}coordinate", nsmap=NSMAP)
    coordinateNode.set("{http://www.opengis.net/swe/1.0}name", self.name)
    self.coordinate.ToXML(coordinateNode)

    return coordinateNode

  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)


class SWEVector(DataType):
  def __init__(self, gmlID, definition, coordinates):
    self.gmlID = gmlID
    self.definition = definition
    #TODO: check array content for SWECoordinate instances!
    #if(isinstance(coordinates, SWECoordinate)):
    self.coordinates = coordinates
    #else:
    #  raise DataTypeException("Vector initialisation: Coordinate information not an instance of Coordinate class!")
    
  
  def ToXML(self, parentNode=None):
    vectorNode = None
    if(parentNode != None):
      vectorNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}Vector", nsmap=NSMAP)
    else:
      vectorNode = etree.Element("{http://www.opengis.net/swe/1.0}Vector", nsmap=NSMAP)
    vectorNode.set("{http://www.opengis.net/gml}id", self.gmlID)
    vectorNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition)
    
    for c in self.coordinates:
      c.ToXML(vectorNode)
    
    return vectorNode
    
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)

    
class SWEObservableProperty(DataType):
  def __init__(self, definition):
    self.definition = definition
  
  def ToXML(self, parentNode=None):
    observablePropertyNode = None
    if(parentNode != None):
      observablePropertyNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}ObservableProperty", nsmap=NSMAP)
    else:
      observablePropertyNode = etree.Element("{http://www.opengis.net/swe/1.0}ObservableProperty", nsmap=NSMAP)
    observablePropertyNode.set("{http://www.opengis.net/swe/1.0}definition", self.definition)
    return observablePropertyNode

  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)
    

class TextBlockEncoding(DataType): #TODO: change to SWE Encoding!
  def __init__(self, decimalSeparator, cellSeparator, rowSeparator):
    self.decimalSeparator = decimalSeparator
    self.cellSeparator = cellSeparator
    self.rowSeparator = rowSeparator
    
  def ToXML(self, parentNode):
    tbeNode = None
    if(parentNode != None):
      tbeNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}TextBlock", nsmap=NSMAP)
    else:
      tbeNode = etree.Element("{http://www.opengis.net/swe/1.0}TextBlock", nsmap=NSMAP)
    tbeNode.set("{http://www.opengis.net/swe/1.0}decimalSeparator", self.decimalSeparator)
    tbeNode.set("{http://www.opengis.net/swe/1.0}cellSeparator", self.cellSeparator)
    tbeNode.set("{http://www.opengis.net/swe/1.0}rowSeparator", self.rowSeparator)
    
    return tbeNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)


class DataArray(DataType):
  def __init__(self, identifier, elementCount, elementCountId, elementTypeName, elementTypeLink, encoding, values):
    self.identifier = identifier
    self.elementCount = elementCount
    self.elementCountId = elementCountId
    self.elementTypeName = elementTypeName
    self.elementTypeLink = elementTypeLink
    self.encoding = encoding #TODO: check if instance of SWE encoding
    self.values = values
    
  def ToXML(self, parentNode = None):
    daNode = None
    if(parentNode != None):
      daNode = etree.SubElement(parentNode, "{http://www.opengis.net/swe/1.0}DataArray", nsmap=NSMAP)
    else:
      daNode = etree.Element("{http://www.opengis.net/swe/1.0}DataArray", nsmap=NSMAP)
    daNode.set("{http://www.opengis.net/gml}id", self.identifier)
    
    ecNode = etree.SubElement(daNode, "{http://www.opengis.net/swe/1.0}elementCount", nsmap=NSMAP)
    cNode = etree.SubElement(ecNode, "{http://www.opengis.net/swe/1.0}Count", nsmap=NSMAP)
    cNode.set("{http://www.opengis.net/gml}id", self.elementCountId)
    cNode.text = self.elementCount
    
    etNode = etree.SubElement(daNode, "{http://www.opengis.net/swe/1.0}elementType", nsmap=NSMAP)
    etNode.set("{http://www.opengis.net/swe/1.0}name", self.elementTypeName)
    etNode.set("{http://www.w3.org/1999/xlink}href", self.elementTypeLink)
    
    encNode = etree.SubElement(daNode, "{http://www.opengis.net/swe/1.0}encoding", nsmap=NSMAP)
    self.encoding.ToXML(encNode)
    
    valNode = etree.SubElement(daNode, "{http://www.opengis.net/swe/1.0}values", nsmap=NSMAP)
    valNode.text = self.values
    
    return daNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)
  
  
  
class TimePosition(DataType):
  def __init__(self, identifier, timePosition, description=None):
    self.identifier = identifier
    self.timePosition = timePosition #TODO: check for "datetime" instance!!
    self.description = description
  
  def ToXML(self, parentNode=None):
    tiNode = None
    if(parentNode != None):
      tiNode = etree.SubElement(parentNode, "{http://www.opengis.net/gml}TimeInstant", nsmap=NSMAP)
    else:
      tiNode = etree.Element("{http://www.opengis.net/om/2.0}TimeInstant", nsmap=NSMAP)
    tiNode.set("{http://www.opengis.net/gml}id", self.identifier)
    
    #TODO: rewrite for "datetime" instance:
    tposNode = etree.SubElement(tiNode, "{http://www.opengis.net/gml}timePosition", nsmap=NSMAP)
    tposNode.text = self.timePosition
    
    if(self.description != None):
      descNode = etree.SubElement(tiNode, "{http://www.opengis.net/gml}description", nsmap=NSMAP)
      descNode.text = self.description
    
    return tiNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)



class TimePeriod(DataType):
  def __init__(self, identifier, periodStart, periodEnd, description=None):
    self.identifier = identifier
    
    #TODO: check for "datetime" instances!!
    self.periodStart = periodStart
    self.periodEnd = periodEnd
    
    self.description = description #TODO: check if standard-compliant
  
  def ToXML(self, parentNode=None):
    tpNode = None
    if(parentNode != None):
      tpNode = etree.SubElement(parentNode, "{http://www.opengis.net/gml}TimePeriod", nsmap=NSMAP)
    else:
      tpNode = etree.Element("{http://www.opengis.net/om/2.0}TimePeriod", nsmap=NSMAP)
    tpNode.set("{http://www.opengis.net/gml}id", self.identifier)
    
    #TODO: rewrite for "datetime" instance:
    psNode = etree.SubElement(tpNode, "{http://www.opengis.net/gml}beginPosition", nsmap=NSMAP)
    psNode.text = self.periodStart
    peNode = etree.SubElement(tpNode, "{http://www.opengis.net/gml}endPosition", nsmap=NSMAP)
    peNode.text = self.periodEnd
    
    if(self.description != None):
      descNode = etree.SubElement(tpNode, "{http://www.opengis.net/gml}description", nsmap=NSMAP)
      descNode.text = self.description
    
    return tpNode
  
  def __str__(self):
    return etree.tostring(self.ToXML(), pretty_print=True)
  